﻿Shader "Custom/RGBAMixColor" {
	Properties {
		_MainTint ("Main Color", Color) = (1, 1, 1, 1)				
		
		_RedColor   ("Red Channel Color",   Color) = (1, 1, 1, 1)
		_GreenColor ("Green Channel Color", Color) = (1, 1, 1, 1)
		_BlueColor  ("Blue Channel Color",  Color) = (1, 1, 1, 1)
		_AlphaColor ("Alpha Channel Color", Color) = (1, 1, 1, 1)
		
		_GrayScaleTexture ("GrayScale Texture", 2D) = "" {}
		_RgbaMask ("RGBA Mask", 2D) = "" {}

		_ContrastControl ("Contrast", Range(0.5,5)) = 1
		_BrightControl ("Bright", Range(0.5,1)) = 0.5
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf Lambert 
		
		float4 _MainTint;				
		float4 _RedColor;
		float4 _GreenColor;
		float4 _BlueColor;
		float4 _AlphaColor;	
			
		float _ContrastControl;
		float _BrightControl;
		
		sampler2D _GrayScaleTexture;
		sampler2D _RgbaMask;

		struct Input {
			float2 uv_GrayScaleTexture;
		};

		/*A , B = color
		float3 overlay(float4 a, float4 b){
			float3 result = 1;
			float3 mult = a*b;
			float3 screen = 1*(1-a)*(1-b);
						
			return result;
        }*/
				
		void surf (Input IN, inout SurfaceOutput o) {
			
			half4 blendData = tex2D (_RgbaMask, IN.uv_GrayScaleTexture);
			half4 mainTexture = tex2D (_GrayScaleTexture, IN.uv_GrayScaleTexture);

			float4 finalColor;
			float4 mainColor = (pow(mainTexture + _BrightControl , _ContrastControl)) * _MainTint;
			
			/* alternativas para controle bright/contrast
			float4 mainColor = (mainTexture.rgba + _BrightControl) *_ContrastControl * _MainTint;
			float4 mainColor = (mainTexture.rgba + _BrightControl ) * (mainTexture.rgba -_ContrastControl) * _MainTint;
			*/
			
			finalColor = _RedColor * (blendData.r);
			finalColor += _GreenColor * (blendData.g);
			finalColor += _BlueColor * (blendData.b);
			finalColor += _AlphaColor * (1-blendData.a);
			finalColor.a = 1;

			finalColor = saturate(finalColor);
			mainColor = saturate(mainColor);

			o.Albedo = finalColor * mainColor;
			//o.Albedo = overlay(mainColor, finalColor);
			o.Alpha = finalColor.a;
		}
		ENDCG
	} 
	FallBack "Diffuse"
}