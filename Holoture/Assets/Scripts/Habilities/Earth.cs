using UnityEngine;
using System;
using System.Collections.Generic;
using System.Text;
using Data;

namespace Habilities{

    /**<summary> Classe que contem comportamentos do ataque de terra</summary>*/
	public class Earth : IAttack{

        /**<summary> Atributo privado do tipo HolotureData que contem dados do ataque </summary>*/
        private AttackData data;

        /**<summary> Proprieda que dá acesso aos dados  do ataque </summary>*/
        public AttackData Data{
            get { return data; }
            set { data = value; }
        }

        /**<summary> Contrutor da classe Earth</summary>*/
        public Earth(){
            data = new AttackData();
            data.type = "earth";
            data.power = 1000;
            data.critical = 15f;
        }

        /**<summary> Metodo que lanca o ataque </summary>*/
        public void Throw(GameObject[] target)
        {
            Debug.Log("Ta atacando pra carai " + this.GetType());
        }

        /**<summary> Metodo que checa se o ataque sera critico. </summary>
        * <param name="criticalRate"> Taxa de critico do ataque </param>
        * <returns> Retorna true se a ataque for critico</returns>*/
        public bool CheckCritical(float criticalRate)
        {
            throw new NotImplementedException();
        }

        /**<summary> Metodo que calcula a força do ataque. </summary>
         * <param name="power"> Atual poder de ataque </param>
         * <returns> Retorna poder de ataque </returns>*/
        public int CalculatePower(int power)
        {
            throw new NotImplementedException();
        }
    }
}
