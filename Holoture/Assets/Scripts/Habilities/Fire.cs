using UnityEngine;
using System;
using System.Collections.Generic;
using System.Text;
using Data;
using Common;

namespace Habilities{

    /**<summary> Classe que contem comportamentos do ataque de fogo</summary>*/
	public class Fire : IAttack{

        /**<summary> Atributo privado do tipo HolotureData que contem dados do ataque </summary>*/
        private AttackData data;

        /**<summary> Proprieda que dá acesso aos dados  do ataque </summary>*/
        public AttackData Data{
            get { return data; }
            set { data = value;}
        }

        /**<summary> Contrutor da classe Fire</summary>*/
        public Fire(){
            data = new AttackData();
            data.type = "fire";
            data.power = 1000;
            data.critical = 15f;
        }

        /**<summary> Metodo que lanca o ataque </summary>*/
        public void Throw(GameObject[] target){
            target[0].GetComponent<Holoture>().TakeDamage(CalculatePower(data.power));
            Debug.Log("Ta atacando pra carai " + this.GetType()+ "POWER " +CalculatePower(data.power));
        }

        /**<summary> Metodo que checa se o ataque sera critico. </summary>
        * <param name="criticalRate"> Taxa de critico do ataque </param>
        * <returns> Retorna true se a ataque for critico</returns>*/
        public bool CheckCritical(float criticalRate){
            System.Random rdm = new System.Random();
            int chance = rdm.Next(0, 101);

            return (chance <= criticalRate);
        }

        /**<summary> Metodo que calcula a força do ataque. </summary>
         * <param name="power"> Atual poder de ataque </param>
         * <returns> Retorna poder de ataque </returns>*/
        public int CalculatePower(int power){
            return (CheckCritical(data.critical) == true) ? power * 2 : power; 
        }
    }
}
