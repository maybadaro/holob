using UnityEngine;
using System;
using System.Collections.Generic;
using System.Text;

namespace Habilities{

    /**<summary> Interface para classes de ataque</summary>*/
	public interface IAttack{
		void Throw(GameObject[] target);
		bool CheckCritical(float criticalRate);
        int CalculatePower(int power);
	}
}
