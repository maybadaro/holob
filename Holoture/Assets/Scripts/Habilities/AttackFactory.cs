using System;
using System.Collections.Generic;
using System.Text;

namespace Habilities
{
	public class AttackFactory : AbstractFactory{

        public override IAttack getAttack(string type){

            if (type == null){
                return null;
            }

            if (type.Equals("air")){
                return new Air();
            }
            else if (type.Equals("earth")){
                return new Earth();
            }
            else if (type.Equals("fire")){
                return new Fire();
            }
            else if (type.Equals("water")){
                return new Water();
            }

            return null;
        }
	}
}
