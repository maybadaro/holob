using System;
using System.Collections.Generic;
using System.Text;

namespace Habilities{

	public class FactoryProducer{

		public static AbstractFactory GetFactory(string choice){
            if (choice.Equals("attack")){
                return new AttackFactory();
            }

            return null;
		}
	}
}
