﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Common;
using GUI;

namespace Managers{

    /**
     * <summary>
     * Classe controladora da cena de batalha.
     * Controla a quantidade de holotures em campo e a ordem de ataque
     * </summary>
     */
    public class BattleManager : MonoBehaviour{

        /**<summary> Vetor de holotures selvagens no campo de batalha </summary>*/
        public GameObject[] savageHolotures;

        /**<summary> Vetor de holotures do palyer no campo de batalha </summary>*/
        public static GameObject[] playerHolotures;

        /**<summary> Lista de holotures(total) do campo de batalha </summary>*/
        public List<GameObject> holotures;

        /**<summary> Fila de açoes da holoture</summary>*/
        private static Queue<String> qActions;

        /**<summary> Controla se batalha já acabou </summary>*/
        public bool endBattle;

        /**<summary> Indica se e inicio de novo turno</summary>*/
        public bool newTurn;

        /**<summary>Propriedade que retorna a quantidade de holotures do player </summary>*/
        public static int HoloturesCount{
            get { return playerHolotures.Length-1; }
        }

        void Awake(){
            endBattle = false;
            newTurn = true;
            savageHolotures = GameObject.FindGameObjectsWithTag("SavageHoloture");
            playerHolotures = GameObject.FindGameObjectsWithTag("Holoture");
            qActions = new Queue<string>();
        }

        void Start() {
            SetHolotures();
            SortHolotures();
        }

        void Update() {
            if (!endBattle){
                if (newTurn){
                    UI_Battle.Instance.ActivateActionButtons();
                    newTurn = false;
                }
                else{
                    try{
                        if (!UI_Battle.Instance.actionButtons.isActiveAndEnabled)
                            StartTurn();

                        Battle();
                    }
                    catch{
                        Debug.Log("fila vazia");
                        newTurn = true;
                    }
                }
            }
        }

        /**<summary> Metodo que preenche lista com todas as holotures e estabele seus alvos</summary>*/
        private void SetHolotures(){
            holotures = savageHolotures.ToList<GameObject>();
            foreach (GameObject o in playerHolotures){
                holotures.Add(o);
            }
        }

        /**<summary> Metodo que estabele os possiveis alvos de cada holoture</summary>*/
        private void SetTargets(){
            foreach (GameObject h in holotures){
                if(h.tag.Equals("Holoture")){
                    h.GetComponent<Holoture>().target = savageHolotures;
                }
                else{
                    h.GetComponent<Holoture>().target = playerHolotures;
                }
            }
        }

        /**<summary> Metodo que reorganiza a lista de holotures pela sua velocidade de ataque, sendo essa a ordem dos ataques durante a batalha </summary>*/
        private void SortHolotures(){
            //usar genericquicksort

            for (int i = 0; i < holotures.Count-1; i++){
                if(holotures[i].GetComponent<Holoture>().Velocity <= holotures[i+1].GetComponent<Holoture>().Velocity){
                    GameObject temp = holotures[i];
                    holotures[i] = holotures[i+1];
                    holotures[i+1] = temp;
                }
            }
        }

        /**<summary> Metodo que enfileira acoes do turno</summary>*/
        public static void EnqueueAction(string action){
            qActions.Enqueue(action);
        }

        /**<summary> Metodo que executa se ainda esta acontecendo a batalha.
         * Caso batalha chegou ao fim e jogador venceu sai da cena de batalha, caso contrario chama GameOver </summary>*/
        private void Battle(){
            foreach (GameObject h in holotures){
                h.GetComponent<Holoture>().CallAction();
            }

            CheckEndBattle();
        }

        /**<summary> Metodo que executa o inicio do turno</summary>*/
        public void StartTurn(){
            foreach (GameObject h in playerHolotures){
                h.GetComponent<Holoture>().Action = qActions.Dequeue();
            }
        }

        /**<summary> Metodo que confere se batalha já acabou</summary>*/
        private void CheckEndBattle(){
            if (playerHolotures == null){
                endBattle = true;//gameover
                Debug.Log("Lose");
                return;
            }
            if(savageHolotures == null){
                endBattle = true;//winbattle
                Debug.Log("WIN");
                return;
            }
        }
    }
}