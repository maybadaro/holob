﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using Common;
using Data;

namespace Managers{
    /**<summary> Classe com funcoes e atributos de controle do jogo</summary>*/
    public class GameManager : MonoBehaviour{

        /**<summary> Atributo que salva instacia da classe</summary>*/
        private static GameManager gameManager;
        /**<summary> Atributo que salva todos os dados do jogo</summary>*/
        private GameData gameData;
        
        /**<summary> Atributo que guarda GameObject Player</summary>*/
        public GameObject player;

        /**<summary> Propriedade que acessa instancia da classe GameManager</summary>*/
        public static GameManager Instance{
            get{
                if (!gameManager){
                    gameManager = FindObjectOfType(typeof(GameManager)) as GameManager;
                }
                return gameManager;
            }
        }

        void Awake(){
            gameManager = this;
            Initialize();
        }

        /**<summary> Propriedade que acessa dados do jogo</summary>*/
        public GameData Game{
            get { return gameData; }
            set { gameData = value; }
        }

        /**<summary> Metodo que inicializa alguns atributos da classe GameManager</summary>*/
        private void Initialize(){
            if (gameData == null){
                gameData = new GameData();
            }
            if (player == null){
                player = GameObject.FindGameObjectWithTag("Player");
            }
        }

        /**<summary> Metodo que salva todas as caracteristicas atuais do jogo(XML)</summary>*/
        public void SaveGame(){
            if (player){
                player.GetComponent<Player>().SavePlayer();
                Game.player = player.GetComponent<Player>().Data;
                XmlSaveLoad.WriteFile(Game, "GameProgressData.xml");
            }
        }

        /**<summary> Metodo que carrega todas as caracteristicas salvas do jogo(XML)</summary>*/
        public void LoadGame(){
            gameData = XmlSaveLoad.Load("GameProgressData.xml");
            if (!player){
                GameObject p = Instantiate(AssetDatabase.LoadAssetAtPath("Assets/Prefabs/Characters/Player.prefab", typeof(GameObject))) as GameObject;
                player = p;
                player.GetComponent<Player>().LoadPlayer();
                player.transform.position = new Vector3(Game.player.character.transformData.positionX, Game.player.character.transformData.positionY, Game.player.character.transformData.positionZ);
                player.transform.rotation = Quaternion.Euler(Game.player.character.transformData.rotationX, Game.player.character.transformData.rotationY,Game.player.character.transformData.rotationZ);
            }
        }
    }
}