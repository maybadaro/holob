﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;

/**<summary> Classe de controle de eventos (Singleton/Observer-Listener)</summary>*/
public class EventManager : MonoBehaviour {

    /**<summary> Atributo que armazena um dicionario de eventos</summary>*/
    private Dictionary<string, UnityEvent> eventDictionary;

    /**<summary> Atributo que armazena a instancia da classe</summary>*/
    private static EventManager eventManager;

    /**<summary> Propriedade que retorna e garante uma unica instancia da classe (Singleton)</summary>*/
    public static EventManager instance{
        get{
            if(!eventManager){
                eventManager = FindObjectOfType(typeof(EventManager)) as EventManager;

                eventManager.Initialize();
            }
            return eventManager;
        }
    }

    /**<summary> Metodo que inicializa o eventDictionary</summary>*/
    private void Initialize(){
        if(eventDictionary == null){
            eventDictionary = new Dictionary<string, UnityEvent>();
        }
    }

    /**<summary> Metodo que adiciona listeners a um evento</summary>
     * <param name="eventName"> Nome do evento </param>
     * <param name="listener"> Listener a ser adicionado</param>*/
    public static void StartListining(string eventName, UnityAction listener){
        UnityEvent thisEvent = null;
        if(instance.eventDictionary.TryGetValue(eventName, out thisEvent)){
            thisEvent.AddListener(listener);
        }
        else{
            thisEvent = new UnityEvent();
            thisEvent.AddListener(listener);
            instance.eventDictionary.Add(eventName, thisEvent);
        }
    }

    /**<summary> Metodo que remove listeners de um evento</summary>
     *<param name="eventName"> Nome do evento </param>
     *<param name="listener"> Listener a ser removido </param>*/
    public static void StopListening(string eventName, UnityAction listener){
        if(eventManager == null) return;

        UnityEvent thisEvent = null;
        if(instance.eventDictionary.TryGetValue(eventName, out thisEvent)){
            thisEvent.RemoveListener(listener);
        }
    }

    /**<summary> Metodo que invoca evento</summary>
     *<param name="eventName"> Nome do evento </param>*/
    public static void TriggerEvent(string eventName){
        UnityEvent thisEvent = null;
        if(instance.eventDictionary.TryGetValue(eventName, out thisEvent)){
            thisEvent.Invoke();
        }
    }
}
