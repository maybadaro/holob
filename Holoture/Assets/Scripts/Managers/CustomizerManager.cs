﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Common;
using Data;
using DataStructure;
using ShaderInteraction;

namespace Managers{

    /**<summary> Classe que contem lista de pecas e cores e funcoes para criacacao de personagens</summary>*/
    class CustomizerManager : MonoBehaviour{

        /**<summary> Atributo que salva instancia da classe</summary>*/
        private static CustomizerManager customizer;
        /**<summary> Variavel para auxiliar no acesso as pecas na lista</summary>*/
        private int auxCount;
        /**<summary> Atributo que guarda objeto player</summary>*/
        private GameObject player;

        /**<summary> Atributo que guarda pecas selecionadas pelo player</summary>*/
        public List<GameObject> playerPieces;
        /**<summary> Atributo que guarda listas de pecas</summary>*/
        public MyLinkedList<List<GameObject>> charPieces = new MyLinkedList<List<GameObject>>();
        /**<summary> Atributo que guarda listas de cores para cada peca</summary>*/
        public MyLinkedList<List<Color>> colors = new MyLinkedList<List<Color>>();
        
        /**<summary> Propriedade que acessa instancia da classe</summary>*/
        public static CustomizerManager Instance{
            get{
                if (!customizer){
                    customizer = FindObjectOfType(typeof(CustomizerManager)) as CustomizerManager;
                    customizer.Initialize();
                }
                return customizer;
            }
        }
        
        //[HideInInspector]
        /**<summary> Atributos que guardam pecas de cada tipo especifico</summary>*/
        public List<GameObject> body,
                                hair,
                                shirt,
                                pants,
                                shoes,
                                backpack;

        //[HideInInspector]
        /**<summary> Atributos que guardam cores de cada tipo especifico de peca</summary>*/
        public List<Color> bodyColor,
                           hairColor,
                           shirtColor,
                           pantsColor,
                           shoesColor,
                           backpackColor;

        void Initialize(){
            player = GameObject.FindGameObjectWithTag("Player");
            auxCount = 0;
            BuildCharList();
            BuildColorList();
        }

        /**<summary> Metodo que popula a lista geral de pecas</summary>*/
        private void BuildCharList(){
            charPieces.Add(body);
            charPieces.Add(hair);
            charPieces.Add(shirt);
            charPieces.Add(pants);
            charPieces.Add(shoes);
            charPieces.Add(backpack);
        }

        /**<summary> Metodo que popula a lista geral de cores</summary>*/
        private void BuildColorList(){
            colors.Add(bodyColor);
            colors.Add(hairColor);
            colors.Add(shirtColor);
            colors.Add(pantsColor);
            colors.Add(shoesColor);
            colors.Add(backpackColor);
        }

        /**<summary> Metodo que instancia Gameobjects de uma lista</summary>
         * <param name="index"> Indice da lista de pecas na lista geral </param>*/
        public void NextPiece(int index){
            charPieces.Begin();

            for (int i = 0; i < index; i++){
                charPieces.Walk();
            }

            List<GameObject> p = charPieces.Access();
            GameObject piece = GameObject.FindGameObjectWithTag(p[0].tag);

            if (piece == null){
                auxCount = 0;
                piece = Instantiate(p[auxCount], player.transform.position, Quaternion.identity) as GameObject;
                piece.transform.parent = player.transform;
            }
            else{
                Destroy(piece);
                auxCount++;
                if (auxCount < p.Count){
                    piece = Instantiate(p[auxCount], player.transform.position, Quaternion.identity) as GameObject;
                    piece.transform.parent = player.transform;
                }
            }
        }

        /**<summary> Metodo que instancia Gameobjects de uma lista</summary>
         * <param name="index"> Indice da lista de pecas na lista geral </param>*/
        public void PreviousPiece(int index){
            charPieces.Begin();
            for (int i = 0; i < index; i++){
                charPieces.Walk();
            }

            List<GameObject> p = charPieces.Access();
            GameObject piece = GameObject.FindGameObjectWithTag(p[0].tag);

            if (piece == null){
                auxCount = p.Count-1;
                piece = Instantiate(p[auxCount], player.transform.position, Quaternion.identity) as GameObject;
                piece.transform.parent = player.transform;
            }
            else{
                Destroy(piece);
                auxCount--;
                if (auxCount >= 0 ){
                    piece = Instantiate(p[auxCount], player.transform.position, Quaternion.identity) as GameObject;
                    piece.transform.parent = player.transform;
                }
            }
        }

        /**<summary> Metodo que seleciona entre os dois corpos</summary>
         * <param name="sex"> male ou female</param>*/
        public void SelectBody(string sex){
            charPieces.Begin();
            List<GameObject> b = charPieces.Access();
            GameObject body = GameObject.FindGameObjectWithTag(b[0].tag);

            if (body != null) Destroy(body);

            if (sex.Equals("male")){
                body = Instantiate(b[0], player.transform.position, Quaternion.identity) as GameObject;
                body.transform.parent = player.transform;
            }
            else{
                body = Instantiate(b[1], player.transform.position, Quaternion.identity) as GameObject;
                body.transform.parent = player.transform;
            }
        }

        /**<summary> Metodo que estabelece uma cor para uma determinada peca</summary>*/
        public void SetColor(string tag, string channel, Color color){
            if (tag.Equals("body")){
                GameObject body = GameObject.FindGameObjectWithTag(tag);
                body.GetComponent<ShaderInteractionHelper>().SetColor(channel, color);
            }
            else{
                int index = playerPieces.IndexOf(GameObject.FindGameObjectWithTag(tag));
                playerPieces[index].GetComponent<ShaderInteractionHelper>().SetColor(channel, color);
            }
        }

         /**<summary> Metodo que preenche uma lista com as pecas que o player selecionou</summary>*/
        public void SetPlayerPieces(){
            for (int i = 0; i < player.transform.childCount; i++){
                playerPieces.Add(player.transform.GetChild(i).gameObject);
            }
        }

        /**<summary> Metodo que apaga as atuais pecas da lista do player</summary>*/
        public void ClearPlayerPieces(){
            playerPieces.Clear();
        }

        /**<summary> Metodo que nomeia player</summary>
         * <param name="name"> Nome do player</param>*/
        public void SetName(string name){
            player.GetComponent<Player>().Name = name;
        }

        /**<summary> Metodo que monta um personagem de forma aleatoria</summary>*/
        public void RandomCharacter(){
            GameObject[] c = GameObject.FindGameObjectsWithTag("Char");
            GameObject p;
            List<PieceData> pdata = new List<PieceData>();
            System.Random rdm = new System.Random();

            foreach (GameObject item in c){
                charPieces.Begin();
                colors.Begin();

                while (!charPieces.TestEnd()){
                    p = Instantiate(charPieces.Access()[rdm.Next(0, charPieces.Access().Count)],item.transform.position,Quaternion.identity) as GameObject;
                    p.transform.parent = item.transform;
                    RandomColor(p);
                    pdata.Add(p.GetComponent<Piece>().Data);
                    charPieces.Walk();
                }
                item.GetComponent<Player>().Pieces = pdata;
            }
        }

        /**<summary> Metodo que escolhe cores aleatoria dentro da lista para um objeto</summary>
         * <param name="obj"> Peca a ser colorida</param>*/
        private void RandomColor(GameObject obj){
            System.Random rdm = new System.Random();

            if (colors.TestEnd())
                colors.Begin();

            obj.GetComponent<ShaderInteractionHelper>().SetColor("_MainTint", colors.Access()[rdm.Next(0, colors.Access().Count)]);
            colors.Walk();
        }

    }
}
