﻿using UnityEngine;
using System;
using System.Collections.Generic;
using DataStructure;
using Data;
using Managers;

namespace Common{

    /**<summary> Classe que contem funcoes de armazenamento e acesso de dados do Player</summary>*/
    class Player : MonoBehaviour{

        /**<summary> Atributo que guarda todos os principais dados do Player</summary>*/
        private PlayerData data;

        void Awake(){
            data = new PlayerData();
        }

        /**<summary> Propriedade que acessa e altera dados do personagem (XML)</summary>*/
        public PlayerData Data{
            get { return data; }
            set { data = value; }
        }

        /**<summary> Propriedade que acessa e altera nome do personagem (XML)</summary>*/
        public string Name{
            get { return data.character.name; }
            set { data.character.name = value; }
        }

        /**<summary> Propriedade que acessa e altera lista de dados das peças que constituem o personagem (XML)</summary>*/
        public List<PieceData> Pieces{
            get { return data.character.pieces; }
            set { data.character.pieces = value; }
        }

        /**<summary> Propriedade que acessa e edita dados de transform do personagem (XML)</summary>*/
        public TransformData TransformData{
            get { return data.character.transformData; }
            set { data.character.transformData = value; }
        }

        /**<summary> Metodo que recebe as transformaçoes atuais do personagem e retorna dado (XML)</summary>
         * <returns> TransformData </returns>*/
        public TransformData SetTransData(){
            data.character.transformData.positionX = this.transform.position.x;
            data.character.transformData.positionY = this.transform.position.y;
            data.character.transformData.positionZ = this.transform.position.z;
            data.character.transformData.rotationX = this.transform.localEulerAngles.x;
            data.character.transformData.rotationY = this.transform.localEulerAngles.y;
            data.character.transformData.rotationZ = this.transform.localEulerAngles.z;
            
            return data.character.transformData;
        }

        /**<summary> Metodo que recebe as peças(gameobject) do personagem e retorna dado (XML)</summary> 
         * <returns> Lista de dados das pecas</returns>*/
        private List<PieceData> SetPieces(){
            List<PieceData> temp = new List<PieceData>();

            if (Pieces.Count == 0){
                int a = this.transform.childCount;
                for (int i = 0; i < a; i++){
                    temp.Add(this.transform.GetChild(i).GetComponent<Piece>().Data);
                }
            }
            else{
                foreach (PieceData p in Pieces){
                    temp.Add(p);
                }
            }
            return temp;
        }

        /**<summary> Metodo que salva todos os dados do Player </summary>*/
        public void SavePlayer(){
            TransformData = SetTransData();
            if (Name == null || Name == " " || Name == ""){
                Name = this.gameObject.name;
            }
            Pieces = SetPieces();
        }

        
        //carrega todos os dados do player e instancia o gameobject
        public void LoadPlayer(){
            Name = GameManager.Instance.Game.player.character.name;
            Pieces = GameManager.Instance.Game.player.character.pieces;
            
            BuildCharacter();
        }

        /**<summary> Metodo que percorre a lista de peças salvas nos dados do player e
         * instancia as peças, buscando nos assests, os prefabs pelo nome </summary>*/
        void BuildCharacter(){
            List<GameObject> p = PopulatePieces();
            int count = Pieces.Count;
            GameObject piece;

            do{
                if (p != null){
                    piece = Instantiate(p[count - 1], this.transform.position, Quaternion.identity) as GameObject;
                    //GameObject piece = Instantiate(bodyPieces.Access(), this.transform.position, Quaternion.identity) as GameObject;
                    piece.transform.parent = this.transform;
                    //Debug.Log(piece.name);
                    //player.pieces.Add(piece.GetComponent<Piece>());
                    //Debug.Log (player.GetPieces(count-1));
                }
                count--;
            } while (count > 0);
        }

        /**<summary>Metodo que procura um objeto pelo nome</summary>
         * <param name="name"> Nome do objeto que se procura</param>
         * <returns> Prefab do objeto procurado</returns>*/
        private GameObject FindGameObj(string name){
            GameObject o = null;
            CustomizerManager.Instance.charPieces.Begin();

            while (!CustomizerManager.Instance.charPieces.TestEnd()){
                for (int i = 0; i < CustomizerManager.Instance.charPieces.Access().Count; i++){
                    if (CustomizerManager.Instance.charPieces.Access()[i].name == name){
                        o = CustomizerManager.Instance.charPieces.Access()[i];
                        return o;
                    }
                }
                CustomizerManager.Instance.charPieces.Walk();
            }
            return o;
            //return UnityEditor.AssetDatabase.LoadAssetAtPath("Assets/Prefabs/Characters/Pieces/" + name + ".prefab", typeof(GameObject)) as GameObject;
        }

        /**<summary> Metodo que recebe informaçao da lista de pecas</summary>
         * <returns> Lista de GameObjects das pecas</returns>*/
        private List<GameObject> PopulatePieces(){
            List<GameObject> p = new List<GameObject>();
            foreach (PieceData item in Pieces){
                p.Add(FindGameObj(item.name));
            }
            return p;
        }

        /**<summary> Metodo informa no console os filhos do GameObject</summary>*/
        public void DebugChild(){
            for (int i = 0; i < this.transform.childCount; i++){
                Debug.Log(this.transform.GetChild(i).name);
            }
        }
    }

    //usar para instanciar prefabs diretamente da pasta
    //Instantiate( Resources.LoadAssetAtPath("Assets/Prefab/Test.prefab", typeof(GameObject)) );
}
