﻿using UnityEngine;
using System.Collections;
using Data;
using Habilities;

namespace Common{

    /**<summary> Classe que controla o acesso aos dados da holotura e possui alguns comportamentos </summary>*/
    public class Holoture : MonoBehaviour
    {

        /**<summary> Atributo privado do tipo HolotureData que contem dados da holotura </summary>*/
        private HolotureData data;

        /**<summary> Atributo privado para instanciar ataque </summary>*/
        private AbstractFactory factory;

        /**<summary> Atributo privado que armazena ataque </summary>*/
        private IAttack attack;

        /**<summary> Atributo publico que armazena o tipo da holoture </summary>*/
        public string type;

        /**<summary> Alvo do ataque/habilidade da holoture</summary>*/
        public GameObject[] target;

        /**<summary> Atributo que diz se a holoture esta ou nao defendendo*/
        public bool defend;

        /**<summary> Atributo que armazena a acao que sera executada</summary>*/
        public string currentAction;

        /**<summary> Propriedade que dá acesso aos dados tanto para leitura quanto para escrita</summary>*/
        public HolotureData Data{
            get { return data; }
            set { data = value; }
        }

        /**<summary> Propriedade que dá acesso ao nome da holoture tanto para leitura quanto para escrita</summary>*/
        public string Name{
            get { return data.name; }
            set { data.name = value; }
        }

        /**<summary> Propriedade que dá acesso a saude da holoture tanto para leitura quanto para escrita </summary>*/
        public int Health{
            get { return data.health; }
            set { data.health = value; }
        }

        /**<summary> Propriedade que dá acesso a defesa da holoture tanto para leitura quanto para escrita </summary>*/
        public int Defense{
            get { return data.defense; }
            set { data.defense = value; }
        }

        /**<summary> Propriedade que dá acesso a taxa de evasao da holoture tanto para leitura quanto para escrita </summary>*/
        public int Evasion{
            get { return data.evasion; }
            set { data.evasion = value; }
        }

        /**<summary> Propriedade que dá acesso a velocidade da holoture tanto para leitura quanto para escrita </summary>*/
        public int Velocity{
            get { return data.velocity; }
            set { data.velocity = value; }
        }

        /**<summary> Propriedade que dá acesso o tipo(fogo, agua, natural, etc) da holoture tanto para leitura quanto para escrita </summary>*/
        public string Type{
            get { return data.type; }
            set { data.type = value; }
        }

        /**<summary> Propriedade que dá acesso a posicao da holoture tanto para leitura quanto para escrita </summary>*/
        public TransformData TransformData{
            get { return data.transform; }
            set { data.transform = value; }
        }

        /**<summary> Propriedade que dá acesso e estabele a acao</summary>*/
        public string Action{
            get { return currentAction; }
            set { currentAction = value; }
        }

        /**<summary> Metodo que armazena as transformaçoes atuais da holoture </summary>
         * <returns> Retorna as transformaçoes do gameobject em forma de dado (HolotureData) </returns>  */
        private TransformData SetTransData(){
            data.transform.positionX = this.transform.position.x;
            data.transform.positionY = this.transform.position.y;
            data.transform.positionZ = this.transform.position.z;
            data.transform.rotationX = this.transform.localEulerAngles.x;
            data.transform.rotationY = this.transform.localEulerAngles.y;
            data.transform.rotationZ = this.transform.localEulerAngles.z;

            return data.transform;
        }

        void Awake(){
            data = new HolotureData();
            factory = FactoryProducer.GetFactory("attack");
            attack = factory.getAttack(type);
            defend = false;
            data.healthMax = 5000;
            Health = data.healthMax;
            Defense = 100;
            Evasion = 10;
            Velocity = 5;
            Type = type;

        }

        void Start(){
        }

        void Update(){
            Debug.Log(this.gameObject.name+ "/" +Health);
        }

        /**<summary> Metodo que calcula a possivel esquiva de um ataque</summary>
         * <returns> Retorna true se holoture conseguira esquivar</returns>*/
        public bool Evade(){
            System.Random rdm = new System.Random();
            return (rdm.Next(0, 101) <= Evasion);
        }

        /**<summary> Metodo que faz a acao defesa ser verdadeira</summary>*/
        public void Defend(){
            defend = true;
        }
        
        /**<summary> Metodo que lanca o poder em um objeto ou em outra holoture</summary>*/
        public void ReleaseAttack(){
            attack.Throw(target);
        }

        /**<summary> Metodo que calcula o dano recebido</summary>
         * <param name="damage"> Parametro do tipo int com o valor do dano que deve ser subtraido a saude atual</param>*/
        public void TakeDamage(int damage){
            if (Evade()) return;
            
            if(defend){
                data.health -= ((damage/2) - Defense);
                defend = false;
            }
            else{
                data.health -= (damage - Defense);
            }

            if (data.health <= 0){
                Dead();
            }
        }

        /**<summary> Metodo que chama sequencia de funçoes quando holoture morreu</summary>*/
        private void Dead(){
            Debug.Log("==MORRÉU==");
            Destroy(this.gameObject);
        }

        /**<summary> Metodo que usa item</summary>*/ 
        //Teste apenas com item de recuperacao de hp
        public void UseItem(){
            Health += Data.healthMax / 2;

            if (Health > Data.healthMax)
                Health = data.healthMax;
        }

        /**<summary> Metodo que chama a acao de acordo com a variavel currentAction</summary>*/
        public void CallAction(){
            switch (currentAction){
                case "attack":
                    ReleaseAttack();
                    break;
                case "defend":
                    Defend();
                    break;
                case "item":
                    UseItem();
                    break;
                default:
                    break;
            }
            currentAction = null;
        }
    }
}