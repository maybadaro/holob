﻿using UnityEngine;
using System;
using System.Collections.Generic;
using Data;
using ShaderInteraction;

namespace Common{

    /**<summary> Classe que contem funcoes e atributos de acesso as principais informacoes de uma peca</summary>*/
    class Piece : MonoBehaviour{
        
        /**<summary> Atributo que guarda informacoes da peca</summary>*/
        private PieceData data;

        /**<summary> Propriedade que acessa e edita dados da peca (XML)</summary>*/
        public PieceData Data{
            get { return data; }
            set { data = value; }
        }

        /**<summary> Propriedade que acessa e edita nome da peca (XML)</summary>*/
        public string Name{
            get { return data.name; }
            set { data.name = value; }
        }

        /**<summary>Propriedade que acessa e edita cor principal da peca (XML)</summary>*/
        public Color MainColor{
            get { return data.mainColor; }
            set { data.mainColor = value; }
        }

        /**<summary>Propriedade que acessa e edita cor do canal r da peca (XML)</summary>*/
        public Color RColor{
            get { return data.rColor; }
            set { data.rColor = value; }
        }

        /**<summary>Propriedade que acessa e edita cor do canal g da peca (XML)</summary>*/
        public Color GColor{
            get { return data.gColor; }
            set { data.gColor = value; }
        }

        /**<summary>Propriedade que acessa e edita cor do canal b da peca (XML)</summary>*/
        public Color BColor{
            get { return data.bColor; }
            set { data.bColor = value; }
        }

        void Awake(){
            data = new PieceData();
            Name = this.gameObject.name;
            LoadColors();
            NormalizeName();
        }

        /**<summary> Metodo que normaliza nome do objeto. Exclui "(Clone)"</summary>*/
        void NormalizeName(){
            char[] n = Name.ToCharArray();
            Name = null;
            for (int i = 0; i < n.Length; i++){
                if (n[i].Equals('(')){
                    break;
                }
                Name += n[i];
            }
        }

        /**<summary> Metodo que salva as cores atuais da peca</summary>*/
        public void SetPieceColor(){
            MainColor = this.GetComponent<ShaderInteractionHelper>().GetColor("_MainTint");
            RColor = this.GetComponent<ShaderInteractionHelper>().GetColor("_RedColor");
            GColor = this.GetComponent<ShaderInteractionHelper>().GetColor("_GreenColor");
            BColor = this.GetComponent<ShaderInteractionHelper>().GetColor("_BlueColor");
            Debug.Log(MainColor);
        }

        public void LoadColors(){
            MainColor = data.mainColor;
            RColor = data.rColor;
            GColor = data.gColor;
            BColor = data.bColor;
        }
    }
}
