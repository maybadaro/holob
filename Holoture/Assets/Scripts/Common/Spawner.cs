﻿using UnityEngine;
using System.Collections;
using Managers;

namespace Common{
    public class Spawner : MonoBehaviour{

        void Start(){
            //Spawn();
            GameManager.Instance.LoadGame();
        }

        void Spawn(){
            CustomizerManager.Instance.RandomCharacter();
        }
    }
}