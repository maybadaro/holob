﻿using UnityEngine;
using System.Collections;

namespace ShaderInteraction{
    public class ShaderInteractionHelper : MonoBehaviour    {

        public Shader current;

        void Start()        {
            current = Shader.Find("Custom/RGBAMixColor");
        }

        //dizer qual cor deve ser mudada: main ou cores secundárias
        public void SetColor(string mrgb, Color c)        {
            this.GetComponent<Renderer>().sharedMaterial.SetColor(mrgb, c);
            //Debug.Log(this.GetComponent<Renderer>().sharedMaterial.GetColor("_MainTint"));
        }

        public Color GetColor(string channel){
            return this.GetComponent<Renderer>().sharedMaterial.GetColor(channel);
        }
    }
}