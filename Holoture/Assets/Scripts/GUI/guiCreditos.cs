﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

/** <summary>
 * Classe utilizada para controlar o menu de creditos.
 * </summary>
 */
public class guiCreditos : MonoBehaviour
{
	/** <summary> Variavel do tipo Button utilizada para controlar um botao. </summary>*/
	public Button voltar;

	/** <summary> Metodo utilizado para inicializar variaveis ou estados quando o jogo começar. </summary>*/
	void Start ()
	{
		voltar = voltar.GetComponent<Button> ();
	}

	/** <summary> Metodo utilizado para carregar uma cena quando o botao eh pressionado. </summary>*/
	public void voltarPressionado()
	{
		Application.LoadLevel (0);
	}
}
