﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

/** <summary>
 * Classe utilizada para controlar o menu jogar.
 * </summary>
 */
public class guiJogar : MonoBehaviour
{
	/** <summary> Variaveis do tipo Button utilizadas para controlar os botoes. </summary>*/
	public Button novoJogo;
	public Button continuar;
	public Button voltar;

	/** <summary> Metodo utilizado para inicializar variaveis ou estados quando o jogo começar. </summary>*/
	void Start ()
	{
		novoJogo = novoJogo.GetComponent<Button> ();
		continuar = continuar.GetComponent<Button> ();
		voltar = voltar.GetComponent<Button> ();
	}

	/** <summary> Metodo utilizado para carregar uma cena quando o botao eh pressionado. </summary>*/
	public void novoJogoPressionado()
	{
		//Application.LoadLevel ();
		Debug.Log ("Carregar a cena de criaçao de personagem.");
	}

	/** <summary> Metodo utilizado para carregar o save do jogo quando o botao eh pressionado. </summary>*/
	public void continuarPressionado()
	{
		Debug.Log ("Carregar o save do jogador.");
	}

	/** <summary> Metodo utilizado para carregar uma cena quando o botao eh pressionado. </summary>*/
	public void voltarPressionado()
	{
		Application.LoadLevel (0);
	}
}
