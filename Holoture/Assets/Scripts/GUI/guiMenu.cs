﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

/** <summary>
 * Classe utilizada para controlar o menu principal.
 * </summary>
 */
public class guiMenu : MonoBehaviour
{
	/** <summary> Variavel do tipo Canvas utilizada para controlar uma janela secundaria. </summary>*/
	public Canvas guiSair;

	/** <summary> Variaveis do tipo Button utilizadas para controlar os botoes. </summary>*/
	public Button jogar;
	public Button opcoes;
	public Button creditos;
	public Button sair;

	/** <summary> Metodo utilizado para inicializar variaveis ou estados quando o jogo começar. </summary>*/
	void Start ()
	{
		guiSair = guiSair.GetComponent<Canvas> ();

		jogar = jogar.GetComponent<Button> ();
		opcoes = opcoes.GetComponent<Button> ();
		creditos = creditos.GetComponent<Button> ();
		sair = sair.GetComponent<Button> ();

		guiSair.enabled = false;
	}

	/** <summary> Metodo utilizado para carregar uma cena quando o botao eh pressionado. </summary>*/
	public void jogarPressionado()
	{
		Application.LoadLevel (1);
	}

	/** <summary> Metodo utilizado para carregar uma cena quando o botao eh pressionado. </summary>*/
	public void opcoesPressionado()
	{
		Application.LoadLevel (2);
	}

	/** <summary> Metodo utilizado para carregar uma cena quando o botao eh pressionado. </summary>*/
	public void creditosPressionado()
	{
		Application.LoadLevel (3);
	}

	/** <summary> Metodo utilizado para habilitar uma janela de opçoes e desabilitar os botoes. </summary>*/
	public void sairPressionado()
	{
		guiSair.enabled = true;

		jogar.enabled = false;
		opcoes.enabled = false;
		creditos.enabled = false;
		sair.enabled = false;
	}

	/** <summary> Metodo utilizado para carregar uma cena quando o botao eh pressionado. </summary>*/
	public void simPressionado()
	{
		Application.Quit ();
	}

	/** <summary> Metodo utilizado para desabilitar uma janela de opçoes e habilitar os botoes. </summary>*/
	public void naoPressionado()
	{
		guiSair.enabled = false;
		
		jogar.enabled = true;
		opcoes.enabled = true;
		creditos.enabled = true;
		sair.enabled = true;
	}
}
