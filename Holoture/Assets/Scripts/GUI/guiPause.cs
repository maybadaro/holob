﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

/** <summary>
 * Classe utilizada para controlar o menu pause.
 * </summary>
 */
public class guiPause : MonoBehaviour 
{
	/** <summary> Variaveis do tipo Button utilizadas para controlar os botoes. </summary>*/
	public Button salvar;
	public Button opcoes;
	public Button menuPrincipal;

	/** <summary> Metodo utilizado para inicializar variaveis ou estados quando o jogo começar. </summary>*/
	void Start ()
	{
		salvar = salvar.GetComponent<Button> ();
		opcoes = opcoes.GetComponent<Button> ();
		menuPrincipal = menuPrincipal.GetComponent<Button> ();
	}

	/** <summary> Metodo utilizado para salvar o jogo quando o botao eh pressionado. </summary>*/
	public void salvarPressionado()
	{
		Debug.Log ("Criar um save do jogo.");
	}

	/** <summary> Metodo utilizado para carregar uma cena quando o botao eh pressionado. </summary>*/
	public void opcoesPressionado()
	{
		Application.LoadLevel (4);
	}

	/** <summary> Metodo utilizado para carregar uma cena quando o botao eh pressionado. </summary>*/
	public void menuPrincipalPressionado()
	{
		Application.LoadLevel (0);
	}
}
