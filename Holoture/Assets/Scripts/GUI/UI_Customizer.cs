﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using System;
using System.Collections;
using Common;
using Managers;
using Modules;

namespace GUI{

    /**<summary> Classe que contem funçoes basicas para a interface de customizacao de personagem</summary>*/
    public class UI_Customizer : MonoBehaviour{

        /**<summary> Atributo que guarda painel de escolha de corpo e nome</summary>*/
        public GameObject chooseSexName;
        /**<summary> Atributo que guarda painel de escolha de pecas do corpo</summary>*/
        public GameObject choosePieces;
        /**<summary> Atributo que guarda painel de escolha de cores para as pecas</summary>*/
        public GameObject chooseColor;
        /**<summary> Atributo que guarda painel de seleção de cores para as pecas especificadas</summary>*/
        public GameObject hColor, sColor, pColor, shColor, bpColor;
        /**<summary> Atributo que guarda painel de finalizacao de personagem</summary>*/
        public GameObject finish;

        /**<summary> Metodo que instancia o corpo</summary>
         * <param name="sex"> male ou female</param>*/
        public void InstantiateBody(string sex){
            //instancia corpo ou manda mensagem?
            Debug.Log(sex);
            CustomizerManager.Instance.SelectBody(sex);
        }

        /**<summary> Metodo que manda cor do botao para a peca</summary>
         * <param name="colorButton"> Botao com a cor a ser enviada</param>*/
        public void SendColor(Button colorButton){
            //manda cor para o corpo
            Color color = colorButton.colors.normalColor;
            string tag = colorButton.transform.parent.name.ToLower();
            string channel = colorButton.tag;
            CustomizerManager.Instance.SetColor(tag,channel,color);
        }

        /**<summary> Metodo que envia nome para o player</summary>
         * <param name="input"> InputField onde sera passado o nome</param>*/
        public void SendName(InputField input){
            //manda o nome para o player
            CustomizerManager.Instance.SetName(input.text);
        }

        /**<summary> Metodo que desativa o painel atual e ativa o proximo painel </summary>
         * <param name="panel"> Panel a ser desativado</param>*/
        public void NextPanel(GameObject panel){

            if (panel == chooseSexName){
                choosePieces.SetActive(true);
            }
            else if (panel == choosePieces){
                chooseColor.SetActive(true);
                hColor.SetActive(true);
                bpColor.SetActive(false);
            }
            else if (panel == chooseColor){
                finish.SetActive(true);
            }
            else if (panel == hColor){
                sColor.SetActive(true);
            }
            else if (panel == sColor){
                pColor.SetActive(true);
            }
            else if (panel == pColor){
                shColor.SetActive(true);
            }
            else if (panel == shColor){
                bpColor.SetActive(true);
            }
            else if (panel == bpColor){
                finish.SetActive(true);
            }
            else if (panel == finish){
                Debug.Log("Finish");
            }
            else{
                throw new Exception("Painel nao existe");
            }
            
            panel.SetActive(false);
        }

        /**<summary> Metodo que desativa o painel atual e ativa o painel anterior </summary>
         * <param name="panel"> Panel a ser desativado</param>*/
        public void BackPanel(GameObject panel){

            if (panel == choosePieces){
                chooseSexName.SetActive(true);
            }
            else if (panel == chooseColor){
                choosePieces.SetActive(true);
            }
            else if (panel == hColor){
                choosePieces.SetActive(true);
            }
            else if (panel == sColor){
                hColor.SetActive(true);
            }
            else if (panel == pColor){
                sColor.SetActive(true);
            }
            else if (panel == shColor){
                pColor.SetActive(true);
            }
            else if (panel == bpColor){
                shColor.SetActive(true);
            }
            else if (panel == finish){
                chooseColor.SetActive(true);
                hColor.SetActive(true);
                bpColor.SetActive(false);
            }
            else{
                throw new Exception("Painel nao existe");
            }

            panel.SetActive(false);
        }

        /**<summary> Metodo que volta ao menu principal</summary>*/
        public void BackMainMenu(){
            //por mensagem?
            //LoadScene.LoadLevel("MainMenu");
            Debug.Log("MainMenu");
        }

        /**Metodo que vai para a cena de inicio do jogo*/
        public void GoGame(){
            //ir para area1 ou pra alguma outra cena?
            LoadScene.LoadLevel("SpawnerTest");
        }

        /**<summary> Metodo que salva players</summary>
         * <param name="player"> GameObject que será salvo</param>*/
        public void SaveCharacter(){
            //salvar direto ou por mensagem?
            for (int i = 0; i < CustomizerManager.Instance.playerPieces.Count; i++){
                CustomizerManager.Instance.playerPieces[i].GetComponent<Piece>().SetPieceColor();
            }
            GameManager.Instance.SaveGame();
            Debug.Log("Salvo");
        }

        /**<summary> Metodo que avanca na lista de pecas</summary>
         * <param name="indexList"> Indice na lista de pecas </param>*/
        public void NextPiece(int indexList){
            //manda mensagem ou muda direto?
            CustomizerManager.Instance.NextPiece(indexList);
        }

        /**<summary> Metodo que volta na lista de pecas</summary>
         * <param name="indexList"> indexList na lista de pecas </param>*/
        public void PreviousPiece(int indexList){
            //manda mensagem ou muda direto?
            CustomizerManager.Instance.PreviousPiece(indexList);
        }

        /**<summary> Metodo que chama uma funcao de salvar pecas</summary>*/
        public void SaveListPieces(){
            CustomizerManager.Instance.SetPlayerPieces();
        }

        /**<summary> Metodo que chama uma funcao que apaga lista de pecas</summary>*/
        public void ClearListPieces(){
            CustomizerManager.Instance.ClearPlayerPieces();
        }
    }
}