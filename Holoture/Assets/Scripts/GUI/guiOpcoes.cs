﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

/** <summary>
 * Classe utilizada para controlar o menu de opçoes.
 * </summary>
 */
public class guiOpcoes : MonoBehaviour
{
	/** <summary> Variavel do tipo Canvas utilizada para controlar uma janela secundaria. </summary>*/
	public Canvas guiControles;

	/** <summary> Variaveis do tipo Button utilizadas para controlar os botoes. </summary>*/
	public Button som;
	public Button efeitos;
	public Button controles;
	public Button voltar;
	
	/** <summary> Metodo utilizado para inicializar variaveis ou estados quando o jogo começar. </summary>*/
	void Start ()
	{
		guiControles = guiControles.GetComponent<Canvas> ();

		som = som.GetComponent<Button> ();
		efeitos = efeitos.GetComponent<Button> ();
		controles = controles.GetComponent<Button> ();
		voltar = voltar.GetComponent<Button> ();

		guiControles.enabled = false;
	}

	/** <summary> Metodo utilizado para desabilitar uma janela de opçoes. </summary>*/
	public void somPressionado()
	{
		Debug.Log ("Configurar ativar e desativar som.");
	}

	/** <summary> Metodo utilizado para desabilitar uma janela de opçoes. </summary>*/
	public void efeitosPressionado()
	{
		Debug.Log ("Configurar ativar e desativar efeitos de som.");
	}

	/** <summary> Metodo utilizado para habilitar uma janela de opçoes e desabilitar os botoes. </summary>*/
	public void controlesPressionado()
	{
		guiControles.enabled = true;
		
		som.enabled = false;
		controles.enabled = false;
		voltar.enabled = false;
	}

	/** <summary> Metodo utilizado para carregar uma cena quando o botao eh pressionado. </summary>*/
	public void voltarPressionado()
	{
		Application.LoadLevel (0);
	}

	/** <summary> Metodo utilizado para desabilitar uma janela de opçoes e habilitar os botoes. </summary>*/
	public void voltar2Pressionado()
	{
		guiControles.enabled = false;
		
		som.enabled = true;
		controles.enabled = true;
		voltar.enabled = true;
	}
}
