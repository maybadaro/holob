﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Common;
using Managers;

namespace GUI{
    
    /**<summary> Classe de controle das funcoes da interface de batalha</summary>*/
    public class UI_Battle : MonoBehaviour{

        /**Atributo que guarda instancia da classe*/
        private static UI_Battle instance;

        /**Propriedade que da acesso a instancia da classe*/
        public static UI_Battle Instance{
            get { return instance; }
        }

        /**<summary> Atributo que armazena o canvas raiz</summary>*/
        public Canvas battleInterface;
        /**<summary> Atributo que armazena o canvas de info das holoture</summary>*/
        public Canvas holoturesInfo;
        /**<summary> Atributo que armazena o canvas com os botoes de acao</summary>*/
        public Canvas actionButtons;

        void Awake(){
            instance = this;
            counterHolotures = 0;
            battleInterface.gameObject.SetActive(false);
            Invoke("ActivateBattleInterface", 2f);
            actionButtons.gameObject.SetActive(false);
        }

        # region CanvasFunctions

        /**<summary> Metodo que ativa interface de batalha</summary>*/
        public void ActivateBattleInterface(){
            if (!battleInterface.isActiveAndEnabled)
                battleInterface.gameObject.SetActive(true);
        }

        /**<summary> Metodo que ativa interface de acoes</summary>*/
        public void ActivateActionButtons(){
            if (!actionButtons.isActiveAndEnabled)
                actionButtons.gameObject.SetActive(true);
        }

        /**<summary> Metodo que desativa interface de acoes</summary>*/
        public void DeactivateActionButtons(){
            if (actionButtons.isActiveAndEnabled)
                actionButtons.gameObject.SetActive(false);
        }
        
        #endregion

        //======================== HoloturesInfo ========================

        /**<summary> Metodo que controla a barra de vida de uma holoture em cena</summary>*/
        public void HealthControl(){
        }

        /**<summary> Metodo que coloca o nome da holoture na barra de vida</summary>*/
        public void SetName(Text name){
            
        }


        //======================== ActionButtons ========================

        /**<summary> Atributo para controle de contagem de holotures </summary>*/
        private int counterHolotures;

        /**<summary> Metodo que estabele ação da holoture</summary>*/
        public void SetAction(string action){
               BattleManager.EnqueueAction(action);
                counterHolotures++;
        
            if (counterHolotures > BattleManager.HoloturesCount){
                counterHolotures = 0;
                DeactivateActionButtons();
            }
        }
    }
}