﻿using UnityEngine;
using System.Collections;

/** <summary>
 * Classe utilizada para controlar o movimento do player.
 * </summary>
 */
public class PlayerMovimento : MonoBehaviour
{
	/** <summary> Variavel do tipo Inteiro que define a velocidade que o player podera se mover. </summary>*/
	public float velocidade = 6f;

	/** <summary> Variavel do tipo Vector3 que armazena a direçao dos movimentos do player. </summary>*/
	public Vector3 movimento;

	/** <summary> Variavel do tipo Animator que refere-se a este componente. </summary>*/
	public Animator animator;

	/** <summary> Variavel do tipo Rigidbody que refere-se ao rigidbody do player. </summary>*/
	public Rigidbody playerRigidbody;

	/** <summary> Metodo utilizado para inicializar variaveis ou estados antes do jogo começar. </summary>*/
	void Awake ()
	{
		animator = GetComponent <Animator> ();
		playerRigidbody = GetComponent <Rigidbody> ();
	}
	
	/** <summary> Metodo chamado a cada quadro framerate. </summary>*/
	void FixedUpdate ()
	{
		float h = Input.GetAxisRaw ("Horizontal");
		float v = Input.GetAxisRaw ("Vertical");
		
		Movimento (h, v);

		Animacao (h, v);
	}

	/** <summary> Metodo utilizado para controlar o movimento do player. </summary>*/
	void Movimento (float h, float v)
	{
		movimento.Set (h, 0f, v);
		
		movimento = movimento.normalized * velocidade * Time.deltaTime;
		
		playerRigidbody.MovePosition (transform.position + movimento);
	}

	/** <summary> Metodo utilizado para controlar as animaçoes. </summary>*/
	void Animacao (float h, float v)
	{
		bool andando = h != 0f || v != 0f;
		
		animator.SetBool ("IsWalking", andando);
	}
}
