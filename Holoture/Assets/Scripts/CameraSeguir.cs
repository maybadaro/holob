﻿using UnityEngine;
using System.Collections;

public class CameraSeguir : MonoBehaviour
{
	/** <summary> Variavel do tipo Transform que define quem a camera estara seguindo. </summary>*/
	public Transform target;

	/** <summary> Variavel do tipo Float que define a velocidade que a camera estara seguindo. </summary>*/
	public float suavizar = 5f;

	/** <summary> Variavel do tipo Vector3 que define o deslocamento inicial do target. </summary>*/
	public Vector3 descolamento; 

	/** <summary> Metodo utilizado para inicializar variaveis ou estados quando o jogo começar. </summary>*/
	void Start ()
	{
		descolamento = transform.position - target.position;
	}

	/** <summary> Metodo chamado a cada quadro framerate. </summary>*/
	void FixedUpdate ()
	{
		Vector3 targetCamPos = target.position + descolamento;
		
		transform.position = Vector3.Lerp (transform.position, targetCamPos, suavizar * Time.deltaTime);
	}
}
