﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace DataStructure{

    /**<summary> Classe da estrutura de dados Lista Duplamente Encadeada, contendo suas principais funçoes</summary>*/
    public class MyLinkedList<T>{

        /**<summary> Atributo que guarda o node inicial</summary>*/
        private Node<T> head;
        /**<summary> Atributo que guarda o node final</summary>*/
        private Node<T> tail;
        /**<summary> Atributo que guarda o node que aponta para outros nodes da lista</summary>*/
        private Node<T> hiker;

        /**<summary> Propriedade que atribui valor ao contador de nodes ou acessa esse valor</summary>*/
        public int Counter { get; private set; }

        /**<summary> Construtor da classe, inicializa os nodes principais e o contador</summary>*/
        public MyLinkedList(){
            head = new Node<T>(default(T), null, null);

            head.previous = head;
            head.next = head;
            tail = head;
            hiker = head;

            Counter = 0;
        }

        /**<summary> Metodo que adiciona um elemento a um node no inicio da lista</summary>
         * <param name="data"> Elemento a ser adicionado </param>*/
        public void AddFirst(T data){
            Node<T> newNode = new Node<T>(data, head, head.next);
            newNode.previous.next = newNode;
            newNode.next = head.next.next;
            Counter++;
        }

        /**<summary> Metodo que adiciona um elemento a um node no fim da lista</summary>
         * <param name="data"> Elemento a ser adicionado </param>*/
        public void Add(T data){
            Node<T> newNode = new Node<T>(data, tail, head);
            newNode.previous.next = newNode;
            newNode.next = head;
            tail = newNode;
            Counter++;
        }

        /**<summary> Metodo que adiciona um elemento a um node no indice indicado da lista</summary>
         *<param name="index"> Indice que pretende-se adicionar elemento</param> 
         *<param name="data"> Elemento a ser adicionado </param>*/
        public void Add(int index, T data){
            if (index >= Size()){
                Add(data);
            }
            else if (index == 0){
                AddFirst(data);
            }
            else{
                Begin();
                for (int i = 0; i < index - 1; i++){
                    Walk();
                }

                Node<T> newNode = new Node<T>(data, hiker, hiker.next);
                newNode.previous.next = newNode;
                newNode.next = hiker.next.next;

                Counter++;
            }
        }

        /**<summary> Metodo que remove da lista a primeira ocorrencia de um elemento</summary>
         *<param name="data"> Elemento a ser removido </param>*/
        public void Remove(T data){
            if (Contains(data)){
                hiker.previous.next = hiker.next;
                hiker.next.previous = hiker.previous;

                Counter--;
            }
        }

        /**<summary> Metodo que remove da lista o elemento no indice especificado</summary>
         *<param name="index"> Indice que pretende-se remover elemento</param> */
        public void RemoveIndex(int index){
            Begin();
            for (int i = 0; i < index - 1; i++){
                Walk();
            }
            hiker.previous.next = hiker.next;
            hiker.next.previous = hiker.previous;

            Counter--;
        }

        /**<summary> Metodo que remove da lista todas as ocorrencia de um elemento especificado</summary>
         *<param name="data"> Elementos a serem removido </param>*/
        public void RemoveAll(T data){
            while (Contains(data)){
                hiker.previous.next = hiker.next;
                hiker.next.previous = hiker.previous;

                Counter--;
            }
        }

        /**<summary> Metodo que procura node com o elemento especificado</summary>
         *<param name="data"> Elemento a ser procurado </param>*/
        public Node<T> Find(T data){
            return (Contains(data)) ? hiker : null;
        }

        /**<summary> Metodo que indica se existe certo elemento na lista</summary>
         * <param name="data"> Elemento a ser procurado</param>
         * <returns> True, quando existe o elemento na lista <returns> */
        public bool Contains(T data){
            Begin();

            while (!TestEnd()){
                if (data.Equals(Access())) return true;
                else Walk();
            }

            return false;
        }

        /**<summary> Metodo que limpa toda a lista </summary>*/
        public void Clear(){
            head.previous = head;
            head.next = head;
            tail = hiker = head;
            Counter = 0;
        }

        /**<summary> Metodo que retorna tamanho da lista</summary>
         * <returns> Numero de nodes na lista</returns>*/
        public int Size(){
            return Counter;
        }

        /**<summary> Metodo que posiciona o node apontador no inicio da lista</summary>*/
        public void Begin(){
            hiker = head.next;
        }

        /**<summary> Metodo que posiciona o node apontador no node da frente</summary>*/
        public void Walk(){
            hiker = hiker.next;
        }

        /**<summary> Metodo que testa se esta no node final da lista</summary>
         * <returns> True, se no node Tail </returns>*/
        public bool TestEnd(){
            return hiker == head;
        }

        /**<summary> Metodo que acessa o elemento do node</summary>
         * <returns> Elemento do node</returns>*/
        public T Access(){
            return hiker.data;
        }

        /**<summary> Metodo que acessa o elemento do ultimo node</summary>
         * <returns> Elemento do node Tail</returns>*/
        public T AccessTail(){
            return tail.data;
        }

        /**<summary> Metodo que retorna o indice de um elemento</summary>
         * <param name="data"> Elemento que prentende-se achar o indice</param>
         * <returns> Indice em que o elemento esta, caso nao exista elemento, retorna -1</returns>*/
        public int GetIndex(T data){
            int count = 0;
            if (Contains(data)){
                Begin();
                while (!data.Equals(Access())){
                    count++;
                    Walk();
                }
                return count;
            }
            return -1;
        }
    }
}
