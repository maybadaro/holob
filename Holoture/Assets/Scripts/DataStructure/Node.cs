﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataStructure{
    /**<summary>Classe Node para diversas estruturas de dados</summary>*/
    public class Node<T>{

        /**<summary> Atributo que guarda dados</summary>*/
        public T data;
        /**<summary> Atributo que guarda node anterior </summary>*/
        public Node<T> previous;
        /**<summary> Atributo que guarda proximo node </summary>*/
        public Node<T> next;

        /**<summary> Construtor da classe Node, inicializa seus atributos</summary>
         * <param name="dt"> Dados que pretende-se salvar no node</param>
         * <param name="nxt"> Node que estara a frente</param>
         * <param name="prev"> Node que estara atras</param>*/
        public Node(T dt, Node<T> prev, Node<T> nxt){
            data = dt;
            previous = prev;
            next = nxt;
        }

    }
}
