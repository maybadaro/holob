﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

/** <summary>
 * Classe utilizada para controlar a vida do player.
 * </summary>
 */
public class PlayerVida : MonoBehaviour
{
	/** <summary> Variavel do tipo Float que define a vida do player quando o jogo inicia. </summary>*/
	public float vidaInicial = 100f;

	/** <summary> Variavel do tipo Float que define a vida atual do player. </summary>*/
	public float vidaAtual;

	/** <summary> Variavel do tipo Float que define o valor total do dano recebido. </summary>*/
	public float danoTotal;

	/** <summary> Variavel do tipo Image que refere-se a UI da barra de vida. </summary>*/
	public Image vidaBarra;

	/** <summary> Variavel do tipo AudioClip que refere-se ao som tocado quando o player morre. </summary>*/
	public AudioClip morteClip;

	/** <summary> Variavel do tipo Animator que refere-se a este componente. </summary>*/
	public Animator animator;

	/** <summary> Variavel do tipo AudioSource que refere-se a este componente. </summary>*/
	public AudioSource playerAudio;

	/** <summary> Variavel do tipo PlayerMovimento que refere-se ao movimento do player. </summary>*/
	public PlayerMovimento playerMovimento;

	/** <summary> Variavel do tipo Bool que verifica se o player esta morto. </summary>*/
	public bool estaMorto = false;

	/** <summary> Variavel do tipo Bool que verifica se o player esta tomando dano. </summary>*/
	public bool dano = false;
	
	/** <summary> Metodo utilizado para inicializar variaveis ou estados antes do jogo começar. </summary>*/
	void Awake ()
	{
		// Setting up the references.
		animator = GetComponent <Animator> ();
		playerAudio = GetComponent <AudioSource> ();
		playerMovimento = GetComponent <PlayerMovimento> ();
		
		vidaAtual = vidaInicial;

	}

	/** <summary> Metodo utilizado para verificar se o player tomou dano e alterar a barra de vida. </summary>*/
	public void ReceberDano (float valorDano)
	{
		dano = true;
		
		danoTotal = (vidaAtual - valorDano) / 100;
		
		iTween.ValueTo(this.gameObject, iTween.Hash("from", this.vidaAtual / 100, "to", danoTotal, "time", 0.5f, "easetype", iTween.EaseType.linear, "onupdate", "AtualizarBarraVida"));

		vidaAtual -= valorDano;

		playerAudio.Play ();
		
		if(vidaAtual <= 0 && !estaMorto)
		{
			Morto ();
		}
	}

	/** <summary> Metodo utilizado para atualizar os valores da imagem que representa a barra de vida. </summary>*/
	void AtualizarBarraVida (float valor)
	{
		vidaBarra.fillAmount = valor;
	}
	
	/** <summary> Metodo utilizado para verificar se o player esta morto. </summary>*/
	void Morto ()
	{
		estaMorto = true;
		
		animator.SetTrigger ("Die");
		
		playerAudio.clip = morteClip;
		playerAudio.Play ();
		
		playerMovimento.enabled = false;
	}  
}
