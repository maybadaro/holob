﻿using UnityEngine;
using System.Collections;

namespace Modules{

    public static class LoadScene{

        public static void LoadLevel(string name){
            Application.LoadLevel(name);
        }
    }
}
