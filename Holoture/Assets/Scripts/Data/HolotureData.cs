﻿using UnityEngine;
using System;
using System.Collections;
using System.Xml;
using System.Xml.Serialization;

namespace Data{

    public class HolotureData{
        public string name;
        public int health;
        public int healthMax;
        public int defense;
        public int evasion;
        public int velocity;
        public string type;
        public TransformData transform;
    }
}