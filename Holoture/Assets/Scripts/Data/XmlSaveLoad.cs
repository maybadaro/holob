﻿using UnityEngine;
using System;
using System.Collections;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Text;
using Managers;

namespace Data{

    public static class XmlSaveLoad{
    
        public static void WriteFile(GameData game, string path) {
            TextWriter writer = new StreamWriter(path);
            XmlSerializer serializer = new XmlSerializer(typeof(GameData));
            game = InitializeGame(game);
            serializer.Serialize(writer, game);
            writer.Close();
        }

        public static GameData Load(string path){
            GameData g = new GameData();

            if (File.Exists(path)){
                XmlSerializer serializer = new XmlSerializer(typeof(GameData));
                TextReader reader = new StreamReader(path);
                g = (GameData)serializer.Deserialize(reader);
                reader.Close();
            }
            else{
                Debug.Log("==File nao Existe==");
            }
            return g;
        }

        public static GameData InitializeGame(GameData game) { 
            GameData g = new GameData();
            g.player = GameManager.Instance.Game.player;
            return g;
        }

    }
}