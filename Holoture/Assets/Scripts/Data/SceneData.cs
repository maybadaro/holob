﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;

namespace Data{

    public class SceneData{

        public string name;
        //objetos de cena
        public List<CharacterData> extraChars;
        public List<EnemyData> enemies;
    }
}