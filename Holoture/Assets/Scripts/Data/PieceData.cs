﻿using UnityEngine;
using System;
using System.Collections;
using System.Xml;
using System.Xml.Serialization;

namespace Data
{
    public class PieceData
    {

        [XmlAttribute("Name")]
        public string name;
        public Color mainColor;
        public Color rColor;
        public Color gColor;
        public Color bColor;
    }
}