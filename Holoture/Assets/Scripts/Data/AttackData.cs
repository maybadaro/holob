﻿using UnityEngine;
using System;
using System.Collections;
using System.Xml;
using System.Xml.Serialization;

namespace Data{

    public struct AttackData{
        public string name;
        public string type;
        public int power;
        public float critical;
    }
}