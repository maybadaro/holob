﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;


namespace Data
{
    public class CharacterData
    {
        public string name;
        public TransformData transformData = new TransformData();

        [XmlArray("Pieces"), XmlArrayItem("Piece")]
        public List<PieceData> pieces = new List<PieceData>();
    }
}
