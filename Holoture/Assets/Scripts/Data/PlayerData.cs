﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;

namespace Data
{
    public class PlayerData
    {

        public CharacterData character = new CharacterData();

        public List<HolotureData> myMonsters = new List<HolotureData>();

        public int money = 100;


    }
}