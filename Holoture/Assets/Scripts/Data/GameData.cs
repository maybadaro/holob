﻿using UnityEngine;
using System;
using System.Collections;
using System.Xml;
using System.Xml.Serialization;

namespace Data
{
    public class GameData
    {

        public PlayerData player;
        public SceneData scene;
        public string dataTime;
    }
}