﻿using UnityEngine;
using System.Collections;

public class Dano : MonoBehaviour {

	GameObject player;

	// Use this for initialization
	void Awake () {
		player = GameObject.FindGameObjectWithTag ("Player");
	}

	void OnTriggerEnter(Collider other){
		if(other.gameObject.tag == "Player"){
			other.gameObject.GetComponent<PlayerVida>().ReceberDano(10);
		}
	}
}
